// Captura dos elementos

var senha = document.querySelector("#senha");
var confirmacao = document.querySelector("#confirmacao");

var primeiroG = document.querySelector(".g:first-child");
var primeiroI = document.querySelector("i:first-child");

var segundoG = document.querySelector(".g:nth-child(2)");
var segundoI = document.querySelector("i:nth-child(3)");

var terceiroG = document.querySelector(".g:nth-child(3)");
var terceiroI = document.querySelector("i:nth-child(5)");

// Evento de escuta

senha.addEventListener("input", function(){

	var valor = senha.value;
	var val = 0;

	var a = senha.value.split("");

	var temN = temNumero(valor);
	var temM = temMaiuscula(valor);

	// Soma das validações

	if(a.length > 5) {
		primeiroI.style.color = "#1ED699";
		val++;
	}
	else {
		primeiroI.style.color = "#F26722";
	}

	if(temM) {
		segundoI.style.color = "#1ED699";
		val++;
	}
	else {
		segundoI.style.color = "#F26722";
	}

	if(temN) {
		terceiroI.style.color = "#1ED699";
		val++;
	}
	else {
		terceiroI.style.color = "#F26722";
	}

	// Lógica de validação

	primeiroG.style.background = "#E5E5E5";
	segundoG.style.background = "#E5E5E5";
	terceiroG.style.background = "#E5E5E5";

	if(val == 1) {
		primeiroG.style.background = "#F26722";
		senha.style.borderColor = "#F26722";
	}
	else if(val == 2) {
		primeiroG.style.background = "#F2B822";
		segundoG.style.background = "#F2B822";
		senha.style.borderColor = "#F2B822";
	}
	else if(val == 3) {
		primeiroG.style.background = "#1ED699";
		segundoG.style.background = "#1ED699";
		terceiroG.style.background = "#1ED699";
		senha.style.borderColor = "#1ED699";
	}
	else {
		senha.style.borderColor = "#F26722";
	}

});

// Funções

var numeros = "0123456789";

function temNumero(texto){
	for(i = 0; i < texto.length; i++){
		if(numeros.indexOf(texto.charAt(i), 0) != -1){
			return 1;
		}
	}
	return 0;
}

var maiusculas = "ABCDEFGHYJKLMNOPQRSTUVWXYZ";

function temMaiuscula(texto) {
	for(i = 0; i < texto.length; i++){
		if(maiusculas.indexOf(texto.charAt(i),0) != -1){
			return 1;
		}
	}
	return 0;
} 